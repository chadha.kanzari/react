import React from "react";
import { Redirect } from "react-router-dom";

import dashboardRoutes from "./views/dashboard/DashboardRoutes";
import utilitiesRoutes from "./views/utilities/UtilitiesRoutes";
import sessionRoutes from "./views/sessions/SessionRoutes";

import materialRoutes from "./views/material-kit/MaterialRoutes";
import dragAndDropRoute from "./views/Drag&Drop/DragAndDropRoute";

import formsRoutes from "./views/forms/FormsRoutes";
import mapRoutes from "./views/map/MapRoutes";
import scrumboardRoute from "../fake-db/db/scrumboardRoute";
import userprofileRoutes from "./views/userprofile/userprofileRoutes";
import customersRoutes from "./views/customers/customersRoutes";
import inboxRoutes from "./views/Inbox/InboxRoutes";
import conventionRoutes from "./views/convention/conventionRoutes";
import CalendarRoutes from "./views/calendar/CalendarRoutes";
import TodoRoutes from "./views/Todo/TodoRoutes";
import tasksRoutes from "./views/tasksgantt/tasksRoutes";
import apptestRoutes from "./views/aaptest/apptestRoutes";
import BoardtaskRoutes from "./views/bryntumtrial/boardtaskRoutes";

const redirectRoute = [
  {
    path: "/",
    exact: true,
    component: () => <Redirect to="/dashboard/analytics" />
  }
];

const errorRoute = [
  {
    component: () => <Redirect to="/session/404" />
  }
];

const routes = [
  ...sessionRoutes,
  ...dashboardRoutes,
  ...materialRoutes,
  ...utilitiesRoutes,
  ...dragAndDropRoute,
  ...formsRoutes,
  ...mapRoutes,
  ...scrumboardRoute,
  ...userprofileRoutes,
  ...customersRoutes,
  ...conventionRoutes,
    ...TodoRoutes,
    ...CalendarRoutes,
  ...inboxRoutes,
    ...tasksRoutes,
    ...apptestRoutes,
    ...BoardtaskRoutes,

  ...redirectRoute,
  ...errorRoute,
];

export default routes;
