export const navigations = [

  {
    name: "Dashboard",
    path: "/dashboard/analytics",
    icon: "dashboard"
  },


  {
    name: "Customers",
    path: "/customers",
    icon: "people",
    children: [

      {
        name: "Set Employees",
        path: "/customers/AddUserList",
        icon: "record_voice_over"
      }
    ]
  },

  {
    name: "Applications ",
    icon: "add_to_queue",
    path: "/application",
    children: [
    {
        name: "Set Applications  ",
        path: "/List",
        icon: "import_contacts"
      },

    ]
  },

  {
    name: "Conventions",
    icon: "assignment",
    children: [

      {
        name: "Set Conventions ",
        path: "/convention/SetConventions",
        icon: "work"
      },

    ]
  },
  {
    name: "Tasks",
    icon: "visibility",
    children: [

      {
        name: "Set Tasks  ",
        path: "/Todo/Todolist",
        icon: "filter_frames"
      },

    ]
  },


  {
    name: "CNI Event Calendar",
    icon: "insert_invitation ",
    children: [{
        name:"Calender Event",
      path: "/calendar/Calendar",
      icon:"watch_later"

}
    ]
  },
  {
    name: "Inbox ",
     icon: "mail_outline ",
    children: [{
      name:"Send Mail",
      path: "/Inbox/InboxRedirect",
      icon: "library_books"
    }]
  },



];
