import jwtAuthService from "../../services/jwtAuthService";
import FirebaseAuthService from "../../services/firebase/firebaseAuthService";
import { setUserData } from "./UserActions";
import history from "history.js";
import firebase from "firebase";
import localStorageService from "../../services/localStorageService";
import "firebase/auth";
import "firebase/database";
import "firebase/storage";
import "firebase/firebase-firestore";
import * as firebaseAuthService from "firebase";
import {getFirestore} from "redux-firestore";
import {getFirebase} from "react-redux-firebase";
import {useDispatch} from "react-redux";
import {toast} from "react-toastify";

export const LOGIN_ERROR = "LOGIN_ERROR";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const SIGNUP_SUCCESS = "SIGNUP_SUCCESS";
export const SIGNUP_ERROR = "SIGNUP_ERROR";

export const LOGIN_LOADING = "LOGIN_LOADING";
export const RESET_PASSWORD = "RESET_PASSWORD";
const auth =firebase.auth();
const provider =new firebase.auth.GoogleAuthProvider();

export function forgotPassword({email}) {
    firebaseAuthService.auth().sendPasswordResetEmail(email)
        .then(function (user) {
            alert('Please check your email...')
        }).catch(function (e) {
        console.log(e)
    })
}

export function resetPassword({email}) {
    return dispatch => {
        dispatch({
            payload: email,
            type: RESET_PASSWORD
        });
    };
}

export function signUpWithEmailAndPassword({email, password}) {
    alert('signUpWithEmailAndPassword called')
    return FirebaseAuthService.createUserWithEmailAndPassword(email, password);
};
export function firebaseLoginEmailPassword({email, password}) {
    return dispatch => {
        FirebaseAuthService.signInWithEmailAndPassword(email, password)
            .then(
                user => { firebase.auth().onAuthStateChanged((u)=>{
                    if(u)
                    {
                        console.log(firebase.auth().currentUser)
                        console.log(u.uid);
                    }
                    if (user) {
                        ////
                        alert("success")
                        user = {
                            userId:u.uid,
                            role: 'GUEST',
                            displayName: email,
                            email: email,
                            photoURL:"/assets/images/admin.png",
                            token: "faslkhfh423oiu4h4kj432rkj23h432u49ufjaklj423h4jkhkjh"
                        }
                        localStorageService.setItem("auth_user", user);
                        localStorageService.setItem("service_user",firebase.auth().currentUser);
                        /////
                        dispatch(
                            setUserData({
                                ...user
                            })
                        );
                        //FirebaseAuthService.signInWithPopup("google")


                        history.push({
                            pathname: "/"
                        });

                        return dispatch({
                            type: LOGIN_SUCCESS
                        });
                    } else {
                        return dispatch({
                            type: LOGIN_ERROR,
                            payload: "Login Failed"
                        });
                    }
                })

                })
            .catch(error => {
                alert("failed");
                return dispatch({
                    type: LOGIN_ERROR,
                    payload: error
                });
            });
    };

}