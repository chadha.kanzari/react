
import firebaseConfig from "./firebaseConfig";
import history from "../../../history";
import * as firebase from "firebase";

class FirebaseAuthService {
  auth;
  firestore;
     database;
     storage;

  googleProvider;
  facebookProvider;
  twitterProvider;

  // // UNCOMMENT IF YOU WANT TO USE FIREBASE
   constructor() {
    this.auth = firebase.auth();
    this.firestore = firebase.firestore().settings({timestampsInSnapshots : true});

    this.database = firebase.database();
    this.storage = firebase.storage();

     this.googleProvider = new firebase.auth.GoogleAuthProvider();

   }



  checkAuthStatus = callback => {
    this.auth.onAuthStateChanged(callback);
  };

  signUpWithEmailAndPassword = (email, password) => {
      alert('signUpWithEmailAndPassword called')

      return this.auth.createUserWithEmailAndPassword(email, password);
  };

  signInWithEmailAndPassword = (email, password) => {
    return this.auth.signInWithEmailAndPassword(email, password);
  };



  signInWithPhoneNumber = phoneNumber => {
    return this.auth.signInWithPhoneNumber(phoneNumber);
  };


  getUserData = docId => {
    //   generally it's better to use uid for docId
    this.firestore
      .collection("users")
      .doc(docId)
      .get()
      .then(doc => {
        console.log(doc.data());
      });
  };

  getAllUser = () => {
    this.firestore
      .collection("users")
      .get()
      .then(docList => {
        docList.forEach(doc => {
          console.log(doc.data());
        });
      });
  };

  signOut = () => {
    return this.auth.signOut();
  };


}

const instance = new FirebaseAuthService();

export default instance;
