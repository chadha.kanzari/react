

// For Firebase JS SDK v7.20.0 and later, measurementId is optional

import "firebase/auth";

import "firebase/database";
import "firebase/storage";
import firebase from "firebase/app";
import "firebase/firestore";
const firebaseConfig = {
    apiKey: "AIzaSyCPUNIk3pzD0z2PIb6-U435V8eqYj14iJU",
    authDomain: "deploy-7db81.firebaseapp.com",
    databaseURL: "https://deploy-7db81-default-rtdb.firebaseio.com",
    projectId: "deploy-7db81",
    storageBucket: "deploy-7db81.appspot.com",
    messagingSenderId: "61731023394",
    appId: "1:61731023394:web:334af063bdc86b7225787b",
    measurementId: "G-21DKDZ7VSZ"
};



firebase.initializeApp(firebaseConfig);
export default firebase;
