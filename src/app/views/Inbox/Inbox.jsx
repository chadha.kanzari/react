import React, {Component, Fragment} from "react";
import { RichTextEditor, Breadcrumb } from "matx";
import {Card, Grid} from "@material-ui/core";
import {Button} from "@material-ui/core";


class Inbox extends Component {
    state = {
        content: `<h1>Matx | Matx Pro Admin Template</h1><p><a href="http://matx-react.ui-lib.com/" target="_blank"><strong>Matx</strong></a></p><p><br></p><p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>`
    };

    handleContentChange = contentHtml => {
        this.setState({
            content: contentHtml
        });
    };

    render() {
        let { theme } = this.props;
        return (
            <div>

                <img src="/assets/images/mail.jpg" alt=""/>
        <Button variant="contained" color="primary" href="http://localhost:3001">Click here to enter your mail box</Button>
            </div>
    );
    }
}

export default Inbox;
