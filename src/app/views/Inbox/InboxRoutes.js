import React from "react";

const inboxRoutes = [
    {
        path: "/Inbox/InboxRedirect",
        component: React.lazy(() => import("./Inbox"))
    },

];

export default inboxRoutes;
