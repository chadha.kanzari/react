import React from "react";

const TodoRoutes = [
    {
        path: "/Todo/Todolist",
        component: React.lazy(() => import("./Todo"))
    },

];

export default TodoRoutes;
