import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './App.css';
import firebase from '../../services/firebase/firebaseConfig';
import {Breadcrumb} from "../../../matx";

class Apptest extends Component {
    constructor(props) {
        super(props);
        this.ref = firebase.firestore().collection('boards');
        this.unsubscribe = null;
        this.state = {
            boards: []
        };
    }

    onCollectionUpdate = (querySnapshot) => {
        const boards = [];
        querySnapshot.forEach((doc) => {
            const { title, description, author } = doc.data();
            boards.push({
                key: doc.id,
                doc, // DocumentSnapshot
                title,
                description,
                author,
            });
        });
        this.setState({
            boards
        });
    }

    componentDidMount() {
        this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
    }

    render() {

        return (
<card >


    <div className="container">
        <div className="mb-sm-30">
            <br></br><br></br><br></br>
            <Breadcrumb
                routeSegments={[
                    {name: "Application", path: "/application"},
                    {name: " Set Applications  "}
                ]}
            /><br></br><br></br>
            <div className="panel panel-default">
                <div className="panel-heading">
                    <h3 className="panel-title">
                        Application List
                    </h3>
                </div>
                <div className="panel-body">
                    <h4><Link to="/create" class="btn btn-primary">Add Application</Link></h4>
                    <table className="table table-stripe">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Type</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.boards.map(board =>
                            <tr>
                                <td><Link to={`/show/${board.key}`}>{board.title}</Link></td>
                                <td>{board.description}</td>
                                <td>{board.author}</td>
                            </tr>
                        )}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</card>



        );
    }
}

export default Apptest;
