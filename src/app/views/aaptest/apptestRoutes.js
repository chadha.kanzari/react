import React from "react";

const apptestRoutes = [
    {
        path: "/List",
        component: React.lazy(() => import("./Apptest"))
    },{
        path: "/create",
        component: React.lazy(() => import("./Create"))
    },{
        path: "/edit/:id",
        component: React.lazy(() => import("./Edit"))
    },
    {
        path: "/show/:id",
        component: React.lazy(() => import("./Show"))
    },



];

export default apptestRoutes;
