import React, { Fragment } from 'react';
import {BryntumDemoHeader,BryntumTaskBoard,BryntumThemeCombo
} from '@bryntum/taskboard-react'
import {taskboardConfig} from "./appconfig";
import './App.scss';
const Boardtask = props => {
    return (
        <Fragment>
            <BryntumDemoHeader
                title="React Simple demo"
                children={<BryntumThemeCombo />}
            />
            <BryntumTaskBoard {...taskboardConfig} />
        </Fragment>
    );
};


export default Boardtask;
