import React, {Component, Fragment} from "react";
import {Card, Grid} from "@material-ui/core";
import {GroupCustomWorkDays} from "../dashboard/shared/CalendarEv"

class AddConv extends Component {

    handleContentChange = contentHtml => {
        this.setState({
            content: contentHtml
        });
    };

    render() {
        let { theme } = this.props;

        return (
            <div className="m-sm-30">
                <div className="analytics m-sm-18 mt--30">


                    <GroupCustomWorkDays/>

                </div>
            </div>
);
    }
}

export default AddConv;
