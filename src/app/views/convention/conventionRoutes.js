import React from "react";

const conventionRoutes = [
    {
        path: "/convention/SetConvention",
        component: React.lazy(() => import("./conventionlist"))
    },
    {
        path: "/convention/SetConventions",
        component: React.lazy(() => import("./conventionlists"))
    }
];

export default conventionRoutes;
