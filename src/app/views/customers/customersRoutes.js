import React from "react";

const customersRoutes = [
    {
        path: "/customers/AddUserList",
        component: React.lazy(() => import("./AddCustomer"))
    }
];

export default customersRoutes;
