import React from "react";

import { SimpleCard, MatxProgressBar } from "matx";

const Campaigns = () => {
  return (
    <div>
      <SimpleCard title="CNI Projects Progress">
        <small className="text-muted">INSAF</small>
        <div className="pt-2" />
        <MatxProgressBar value={75} color="primary" text="Marketing working" />
        <div className="py-1" />
        <MatxProgressBar value={45} color="secondary" text="Development and test" />
        <div className="py-1" />
        <MatxProgressBar value={75} color="primary" text="Deployment" />

        <div className="py-3" />
        <small className="text-muted">ADAB</small>
        <div className="pt-2" />
        <MatxProgressBar value={75} color="primary" text="Google (102k)" />
        <div className="py-1" />
        <MatxProgressBar value={45} color="secondary" text="Twitter (40k)" />
        <div className="py-1" />
        <MatxProgressBar value={75} color="primary" text="Tensor (80k)" />

        <div className="py-3" />
        <small className="text-muted">Yesterday</small>
        <div className="pt-2" />
        <MatxProgressBar value={75} color="primary" text="Google (102k)" />
        <div className="py-1" />
        <MatxProgressBar value={45} color="secondary" text="Twitter (40k)" />
        <div className="py-1" />
        <MatxProgressBar value={75} color="primary" text="Tensor (80k)" />
      </SimpleCard>
    </div>
  );
};

export default Campaigns;
