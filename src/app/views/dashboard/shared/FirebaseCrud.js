import React, {Component, useEffect, useState} from "react";
import {Container, Form, Grid, Header, Input, Segment, Table,Icon,Button} from "semantic-ui-react";
import {Breadcrumb} from "../../../../matx";
import firebase from "../../../services/firebase/firebaseConfig"
import App from "../shared/App.css"
import 'semantic-ui-css/semantic.min.css'

const FirebaseCrud = () =>{

    const [aFirstName,setAFirstName]= useState('');
    const [aemail,setaemail]= useState('');
    const [apassword,setapassword]= useState('');

    const [aLastName,setalastName]= useState('');
    const [aTel,setaTel]= useState('');

    const [userData,setUserData]=useState([]);
    const[uFirstName,setuFirstName]=useState('');
    const[uTel,setuTel]=useState('');

    const[uemail,setuemail]=useState('');
    const[upassword,setupassword]=useState('');

    const[uLastName,setuLastName]=useState('');
    const[userId,setUserId]=useState('');
useEffect(()=>{
    const  firestore = firebase.database().ref("/UserInfo");
firestore.on('value',(response)=>{
    const data = response.val();
     let userInfo =[];
    for (let id in data){
          userInfo.push ({
            id: id ,
            FirstName: data[id].FirstName,
            LastName: data[id].LastName,
              Email: data[id].Email,
              Password: data[id].Password,
              Tel:data[id].Tel,


          });
    }
    setUserData(userInfo);
});
},[]);
const handleAddUser = ()=>{
const firestore = firebase.database().ref("/UserInfo");

let data ={
    FirstName : aFirstName,
    LastName : aLastName,
    Email:aemail,
    Password:apassword,
    Tel:aTel

}
        firestore.push(data)
    firebase.auth().createUserWithEmailAndPassword(aemail,apassword).then((u)=>{console.log(u)}).catch((err)=>{console.log(err)})

    setAFirstName("");
    setalastName("");
    setaemail("");
    setapassword("");
    setaTel("");
    console.log(data)

}


const handleUpdateUser = ()=>{
    const  firestore = firebase.database().ref("/UserInfo").child(userId);
firestore.update({
    FirstName: uFirstName,
    LastName: uLastName,
    Email:uemail,
    Password:upassword,
    Tel:uTel
})
    setuFirstName("");
setuLastName("");
setuTel("");
};

const handleDelete = (id) =>{
    const  firestore = firebase
        .database()
        .ref("/UserInfo")
        .child(id);
    firestore.remove();
};

    const handleUpdateClick = (data)=> {
        console.log(data);
        setuFirstName(data.FirstName);
        setuLastName(data.LastName);
        setupassword(data.Password);
        setuemail(data.Email);
        setuTel(data.Tel);
        setUserId(data.id);
    };

    return(
        <div  className="mb-sm-30">
        <br></br><br></br><br></br><br></br><br></br>
        <Breadcrumb
            routeSegments={[
                { name: "Admins", path: "/Admin" },
                { name: " Manage Admins  " }
            ]}
        /><br></br><br></br>


        <div>
            <Container>
                <Grid>
                    <Grid.Row columns ="2">
                        <Grid.Column>
                            <Segment padded="very">
                                <Form>
                                    <Form.Field>
                                        <label>FirstName</label>
                                        <Input required placeholder="FirstName" value={aFirstName}
                                               onChange={ (e)=>{setAFirstName(e.target.value); }} />
                                    </Form.Field>
                                    <Form.Field>
                                        <label>
                                            LastName
                                        </label>

                                        <Input required placeholder="LastName" value={aLastName}
                                               onChange={ (e)=>{setalastName(e.target.value); }} />


                                    </Form.Field>
                                    <Form.Field>
                                        <label>
                                            Tel
                                        </label>

                                        <Input required placeholder="Tel" value={aTel}
                                               type="number" onChange={ (e)=>{setaTel(e.target.value); }} />


                                    </Form.Field>
                                    <Form.Field>
                                        <label>
                                            Email
                                        </label>

                                        <Input type="email" pattern="@" required placeholder="Email" value={aemail}
                                               onChange={ (e)=>{setaemail(e.target.value); }} />


                                    </Form.Field>
                                    <Form.Field>
                                        <label>
                                            Password
                                        </label>

                                        <Input required placeholder="Password" value={apassword} type="password"
                                               onChange={ (e)=>{setapassword(e.target.value); }} />


                                    </Form.Field>
                                    <Form.Field>
                                        <Button onClick={()=>{handleAddUser()}}positive><Icon name="add circle"></Icon>
                                              Add User
                                        </Button>
                                    </Form.Field>
                                </Form>
                            </Segment>
                        </Grid.Column>
                            <Grid.Column>     <Segment padded="very">
                                <Form>
                                    <Form.Field>
                                        <label>FirstName</label>
                                        <Input required placeholder="FirstName" focus value={uFirstName}
                                               onChange={ (e)=>{setuFirstName(e.target.value); }} />
                                    </Form.Field>
                                    <Form.Field>
                                        <label>
                                            LastName
                                        </label>

                                        <Input required placeholder="LastName" focus value={uLastName}
                                               onChange={ (e)=>{setuLastName(e.target.value); }} />


                                    </Form.Field>
                                    <Form.Field>
                                        <label>
                                            Tel
                                        </label>

                                        <Input required placeholder="Tel" focus value={uTel}
                                              type="number" onChange={ (e)=>{setuTel(e.target.value); }} />


                                    </Form.Field>
                                    <Form.Field>
                                        <Button onClick={()=>{handleUpdateUser()}}primary >

                                            <Icon name="edit"></Icon>  Update User
                                        </Button>
                                    </Form.Field>
                                </Form>
                            </Segment> </Grid.Column>
                    </Grid.Row>
               <Grid.Row columns="1">
               <Grid.Column>
                   {
                       userData.length === 0 ? (
                           <Segment>
                               <Header textAlign="center">
                                 Oops ! No data available
                               </Header>
                           </Segment>
                       ):(
                           <Segment>
                               <Table celled fixed singleLine>
                                    <Table.Header>
                                        <Table.Row>
                                            <Table.HeaderCell>First Name </Table.HeaderCell>
                                            <Table.HeaderCell>Last Name </Table.HeaderCell>
                                            <Table.HeaderCell>Email </Table.HeaderCell>
                                            <Table.HeaderCell>Tel </Table.HeaderCell>


                                            <Table.HeaderCell> </Table.HeaderCell>
                                        </Table.Row>
                                    </Table.Header>
                                   {
                                       userData.map((data, index) => {
                                       return(
                                       <Table.Body>
                                       <Table.Cell>{data.FirstName}
                                       </Table.Cell>
                                       <Table.Cell>{data.LastName}
                                       </Table.Cell>
                                           <Table.Cell>{data.Email}
                                           </Table.Cell>
                                           <Table.Cell>{data.Tel}
                                       </Table.Cell>

                                       <Table.Cell>
                                       <Button primary onClick={()=>{handleUpdateClick(data)}}>
                                       <Icon name ="edit"></Icon>

                                       </Button>
                                       <Button color="red" onClick={()=> handleDelete(data.id)}>
                                       <Icon name ="delete"></Icon>

                                       </Button>
                                       </Table.Cell>
                                       </Table.Body>
                                       );
                                   })
                                   }
                               </Table>
                           </Segment>
                       )
                   }
               </Grid.Column>
               </Grid.Row>





            </Grid>



            </Container>
        </div>
        </div>

    )



}
export default FirebaseCrud ;