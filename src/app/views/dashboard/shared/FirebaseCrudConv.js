import React, {Component, useEffect, useState} from "react";
import {Container, Form, Grid, Header, Input, Segment, Table,Icon,Button} from "semantic-ui-react";
import {Breadcrumb} from "../../../../matx";
import firebase from "../../../services/firebase/firebaseConfig"
import 'semantic-ui-css/semantic.min.css'

const FirebaseCrudApp = () =>{
    const [aAppName,setAAppName]= useState('');
    const [aAppRef,setAAppRef]= useState('');
    const [aStatus,setAStatus]= useState('');
    const [aDetails,setaDetails]= useState('');
    const [appData,setAppData]=useState([]);
    const[uAppName,setuAppName]=useState('');
    const[uAppRef,setuAppRef]=useState('');
    const[uStatus,setuStatus]=useState('');

    const[uDetails,setuDetails]=useState('');
    const[appId,setAppId]=useState('');
        useEffect(()=>{
            const  firestore = firebase.database().ref("/AppInfo");
            firestore.on('value',(response)=>{
                const data = response.val();
                let appInfo =[];
                for (let id in data){
                    appInfo.push ({
                        id: id ,
                        AppName: data[id].AppName,
                        Details: data[id].Details,
                        References:data[id].References,
                        Status:data[id].Status,

                    });
                }
                setAppData(appInfo);
            });

    },[]);
    const handleAddApp = ()=>{
        const firestore = firebase.database().ref("/AppInfo");

        let data ={
            AppName : aAppName,
            Details: aDetails,
            References:aAppRef,
            Status:aStatus,

        }
        firestore.push(data)
        setAAppName("");
        setaDetails("")
        setAAppRef("")
        setAStatus("")
        console.log(data)
    }


    const handleUpdateApp = ()=>{
        const  firestore = firebase.database().ref("/AppInfo").child(appId);
        firestore.update({
            AppName: uAppName,
            Details: uDetails,
            References:uAppRef,
            Status:uStatus,
        })
        setuAppName("");
        setuDetails("");
        setuAppRef("");
        setuStatus("");

    };

    const handleDelete = (id) =>{
        const  firestore = firebase
            .database()
            .ref("/AppInfo")
            .child(id);
        firestore.remove();
    };

    const handleUpdateClick = (data)=> {
        console.log(data);
        setuAppName(data.AppName);
        setuDetails(data.Details);
        setuAppRef(data.References);
        setuStatus(data.Status);
        setAppId(data.id);
    };

    return(
        <div  className="mb-sm-30">
            <br></br><br></br><br></br><br></br><br></br>
            <Breadcrumb
                routeSegments={[
                        { name: "Applications", path: "/application" },
                    { name: " Set applications " }
                ]}
            /><br></br><br></br>


            <div>
                <Container>
                    <Grid>
                        <Grid.Row columns ="2">
                            <Grid.Column>
                                <Segment padded="very">
                                    <Form>
                                        <Form.Field>
                                            <label>Application Name</label>
                                            <Input placeholder="Enter application name" value={aAppName}
                                                   onChange={ (e)=>{setAAppName(e.target.value); }} />
                                        </Form.Field>
                                        <Form.Field>
                                            <label>
                                                Details
                                            </label>

                                            <Input placeholder="now add details" value={aDetails}
                                                   onChange={ (e)=>{setaDetails(e.target.value); }} />


                                        </Form.Field>
                                        <Form.Field>
                                            <label>
                                                References
                                            </label>

                                            <Input placeholder="now add Refrence App" value={aAppRef}
                                                   onChange={ (e)=>{setAAppRef(e.target.value); }} />


                                        </Form.Field>
                                        <Form.Field>
                                            <label>
                                            Status
                                            </label>

                                            <Input placeholder="don't forget to set the Availability App" value={aStatus}
                                                   onChange={ (e)=>{setAStatus(e.target.value); }} />


                                        </Form.Field>


                                        <Form.Field>
                                            <Button onClick={()=>{handleAddApp()}}positive><Icon name="add circle"></Icon>
                                                Add Application
                                            </Button>
                                        </Form.Field>
                                    </Form>
                                </Segment>
                            </Grid.Column>
                            <Grid.Column>
                                <Segment padded="very">
                                <Form>
                                    <Form.Field>
                                        <label>
                                            Details
                                        </label>

                                        <Input placeholder=" Set Application Details" focus value={uDetails}
                                               onChange={ (e)=>{setuDetails(e.target.value); }} />


                                    </Form.Field>

                                    <Form.Field>
                                        <label>
                                            Status
                                        </label>

                                        <Input placeholder=" Set Application Status " focus value={uStatus}
                                               onChange={ (e)=>{setuStatus(e.target.value); }} />


                                    </Form.Field>

                                    <Form.Field>
                                        <Button onClick={()=>{handleUpdateApp()}}primary >

                                            <Icon name="edit"></Icon>  Update Application
                                        </Button>
                                    </Form.Field>
                                </Form>
                            </Segment> </Grid.Column>
                        </Grid.Row>
                        <Grid.Row columns="1">
                            <Grid.Column>
                                {
                                    appData.length === 0 ? (
                                        <Segment>
                                            <Header textAlign="center">
                                                Oops ! No data available
                                            </Header>
                                        </Segment>
                                    ):(
                                        <Segment>
                                            <Table celled fixed singleLine >
                                                <Table.Header>
                                                    <Table.Row>
                                                        <Table.HeaderCell width="2" >Application Name</Table.HeaderCell>
                                                        <Table.HeaderCell width="2">Details </Table.HeaderCell>
                                                        <Table.HeaderCell width="2">References </Table.HeaderCell>
                                                        <Table.HeaderCell width="1">Status </Table.HeaderCell>
                                                        <Table.HeaderCell width="2"> </Table.HeaderCell>
                                                    </Table.Row>
                                                </Table.Header>
                                                {
                                                    appData.map((data, index) => {
                                                        return(
                                                            <Table.Body>
                                                                <Table.Cell>{data.AppName}
                                                                </Table.Cell>
                                                                <Table.Cell>{data.Details}
                                                                </Table.Cell>
                                                                <Table.Cell>{data.References}
                                                                </Table.Cell>
                                                                <Table.Cell>{data.Status}
                                                                </Table.Cell>


                                                                <Table.Cell >
                                                                    <Button primary onClick={()=>{handleUpdateClick(data)}}>
                                                                        <Icon name ="edit"></Icon>

                                                                    </Button>
                                                                    <Button color="red" onClick={()=> handleDelete(data.id)}>
                                                                        <Icon name ="delete"></Icon>

                                                                    </Button>
                                                                </Table.Cell>
                                                            </Table.Body>
                                                        );
                                                    })
                                                }
                                            </Table>
                                        </Segment>
                                    )
                                }
                            </Grid.Column>
                        </Grid.Row>





                    </Grid>



                </Container>
            </div>
        </div>

    )



}
export default FirebaseCrudApp ;