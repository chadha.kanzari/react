import React from "react";
import { Grid, Card, Icon, IconButton, Tooltip } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";

const styles = theme => ({
  icon: {
    fontSize: "44px",
    opacity: 0.6,
    color: theme.palette.primary.main
  }
});

const StatCards = ({ classes }) => {
  return (
    <Grid container spacing={3} className="mb-3">
      <Grid item xs={12} md={6}>
        <Card className="play-card p-sm-24 bg-paper" elevation={6}>
          <div className="flex items-center">
            <Icon className={classes.icon}>group</Icon>
            <div className="ml-3">
              <small className="text-muted">Recruitment</small>
              <h6 className="m-0 mt-1 text-primary font-medium">Annual competitions</h6>
            </div>
          </div>
          <Tooltip title="CNI privileges" placement="top">
            <IconButton>
              <Icon>done_all</Icon>
            </IconButton>
          </Tooltip>
        </Card>
      </Grid>
      <Grid item xs={12} md={6}>
        <Card className="play-card p-sm-24 bg-paper" elevation={6}>
          <div className="flex items-center">
            <Icon className={classes.icon}>attach_money</Icon>
            <div className="ml-3">
              <small className="text-muted">Profitability</small>
              <h6 className="m-0 mt-1 text-primary font-medium">$80,500</h6>
            </div>
          </div>
          <Tooltip title="CNI's gain profit" placement="top">
            <IconButton>
              <Icon>wb_sunny</Icon>
            </IconButton>
          </Tooltip>
        </Card>
      </Grid>
      <Grid item xs={12} md={6}>
        <Card className="play-card p-sm-24 bg-paper" elevation={6}>
          <div className="flex items-center">
            <Icon className={classes.icon}>cast_connected</Icon>
            <div className="ml-3">
              <small className="text-muted">Application Status</small>
              <h6 className="m-0 mt-1 text-primary font-medium">
                99% of the applications are available
              </h6>
            </div>
          </div>
          <Tooltip title="Application's availability" placement="top">
            <IconButton>
              <Icon>done_all</Icon>
            </IconButton>
          </Tooltip>
        </Card>
      </Grid>
      <Grid item xs={12} md={6}>
        <Card className="play-card p-sm-24 bg-paper" elevation={6}>
          <div className="flex items-center">
            <Icon className={classes.icon}>rv_hookup</Icon>
            <div className="ml-3">
              <small className="text-muted">Spot tracking
              </small>
              <h6 className="m-0 mt-1 text-primary font-medium">Daily check application updates  </h6>
            </div>
          </div>
          <Tooltip title="CNI's service efficiency" placement="top">
            <IconButton>
              <Icon>done</Icon>
            </IconButton>
          </Tooltip>
        </Card>
      </Grid>
    </Grid>
  );
};

export default withStyles(styles, { withTheme: true })(StatCards);
