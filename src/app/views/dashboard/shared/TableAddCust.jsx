import React, {Component, useState} from "react";
import {
    Card,
    Checkbox,
    FormControlLabel,
    Grid,
    Button
} from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { connect } from "react-redux";
import * as PropTypes from "prop-types";
import {withStyles} from "@material-ui/core/styles";
import {withRouter} from "react-router-dom";
import {auth} from "firebase";
import{styles} from "@material-ui/pickers/views/Clock/Clock";
import firebase from "../../../services/firebase/firebaseConfig";
class SignUp extends Component {
    state = {
        username: "",
        email: "",
        password: "",
        role:"",
        agreement: ""
    };



    handleFormSubmit = event => {
        alert('submit')
        this.props.createUserWithEmailAndPassword(this.state.email,this.state.password).then()
    };
    render() {
        let { username, email, password,role } = this.state;
        return (
            <div className="signup flex justify-center w-full h-full-screen">
                <div className="p-8">
                    <Card className="signup-card position-relative y-center">
                        <Grid container>
                            <Grid item lg={5} md={5} sm={5} xs={12}>
                                <div className="p-8 flex justify-center bg-light-gray items-center h-full">
                                    <img
                                        src="/assets/images/illustrations/posting_photo.svg"
                                        alt=""
                                    />
                                </div>
                            </Grid>
                            <Grid item lg={7} md={7} sm={7} xs={12}>
                                <div className="p-9 h-full">
                                    <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
                                        <TextValidator
                                            className="mb-6 w-full"
                                            variant="outlined"
                                            label="Username"
                                            type="text"
                                            name="username"
                                            value={username}
                                            validators={["required"]}
                                            errorMessages={["this field is required"]}
                                        />
                                        <TextValidator
                                            className="mb-6 w-full"
                                            variant="outlined"
                                            label="Email"
                                            type="email"
                                            name="email"
                                            value={email}
                                            validators={["required", "isEmail"]}
                                            errorMessages={[
                                                "this field is required",
                                                "email is not valid"
                                            ]}
                                        />
                                        <TextValidator
                                            className="mb-4 w-full"
                                            label="Password"
                                            variant="outlined"
                                            name="password"
                                            type="password"
                                            value={password}
                                            validators={["required"]}
                                            errorMessages={["this field is required"]}
                                        />
                                        <TextValidator
                                            className="mb-6 w-full"
                                            variant="outlined"
                                            label="Role"
                                            type="text"
                                            name="role"
                                            value={role}
                                            validators={["required"]}
                                            errorMessages={["this field is required"]}
                                        />
                                        <FormControlLabel
                                            className="mb-4"
                                            name="agreement"
                                            control={<Checkbox />}
                                            label="I have read and agree to the terms of service."
                                        />
                                        <div className="flex items-center">
                                            <Button
                                                className="capitalize"
                                                variant="contained"
                                                color="primary"
                                                onClick={this.createcust}
                                            >
                                                Add Customer
                                            </Button>

                                        </div>
                                    </ValidatorForm>
                                </div>
                            </Grid>
                        </Grid>
                    </Card>
                </div>
            </div>
        );
    }
}



//export default connect(mapStateToProps, {})(SignUp);
export default SignUp