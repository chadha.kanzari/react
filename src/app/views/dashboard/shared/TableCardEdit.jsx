import React, { Component } from "react";
import {
    Card,
    Checkbox,
    FormControlLabel,
    Grid,
    Button
} from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { connect } from "react-redux";
import * as PropTypes from "prop-types";
import {withStyles} from "@material-ui/core/styles";
import {auth} from "firebase";
import{signUpWithEmailAndPassword} from "../../../redux/actions/LoginActions";
import{styles} from "@material-ui/pickers/views/Clock/Clock";
import {Breadcrumb} from "../../../../matx";
import axios from "axios";
import * as moment from "moment";
import AppIcon from "../../material-kit/icons/AppIcon";
import EditIcon from "@material-ui/icons/Edit";

class TableAddApp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            reference: "",
            details: "",
            active:"",
            date_expiration: "",
        };

    }
    handleInputChange = (e) =>{
        const {name, value} = e.target;
        this.setState({
            ...this.state,
            [name]:value,
        });
    };



    onSubmit =(e) =>{
        e.preventDefault();
        const {name, reference,active, details,date_expiration} = this.state;
        const data = {
            name: name,
            details: details,
            reference:reference,
            active:active,
            date_expiration : date_expiration
        };
        console.log(data.date_expiration)
        axios.put("http://localhost:8080/api/v2/update/{id}",data)
            .then((res)=>{
                if(res.data.success){
                    alert("Added");
                    this.setState({name:"", details:"", reference:"" ,active:"", date_expiration:""});
                }
            });

    };


    render() {
        return (
            <div  className="mb-sm-30">
                <br></br><br></br><br></br><br></br><br></br>
                <Breadcrumb
                    routeSegments={[
                        { name: "Application", path: "/application" },
                        { name: "Edit Application  " }
                    ]}
                /><br></br>
                <div className="signup flex justify-center w-full h-full-screen">
                    <div className="p-0">
                        <Card className="signup-card position-relative y-center">
                            <Grid container justify="center">
                                <Grid item lg={5} md={5} sm={5} xs={12}>
                                    <div className="p-8 flex justify-center bg-light-gray items-center h-full">
                                        <img
                                            src="/assets/images/illustrations/mail1.jpg"
                                            alt=""
                                        />
                                    </div>
                                </Grid>
                                <Grid item lg={7} md={7} sm={7} xs={12}>
                                    <div className="p-9 h-full">
                                        <ValidatorForm ref="form" >
                                            <TextValidator
                                                className="mb-6 w-full"
                                                variant="outlined"
                                                label="Product Name"
                                                onChange={this.handleInputChange}
                                                type="text"
                                                name="name"
                                                value={this.state.name}
                                                validators={["required"]}
                                                errorMessages={["this field is required"]}
                                            />

                                            <TextValidator
                                                className="mb-4 w-full"
                                                label=" Reference N°"
                                                variant="outlined"
                                                onChange={this.handleInputChange}
                                                name="reference"
                                                type="phone"
                                                value={this.state.reference}
                                                validators={["required"]}
                                                errorMessages={["this field is required"]}
                                            />
                                            <TextValidator
                                                className="mb-6 w-full"
                                                variant="outlined"
                                                label="Details"
                                                onChange={this.handleInputChange}
                                                type="Text"
                                                name="details"
                                                value={this.state.details}
                                                validators={["required"]}
                                                errorMessages={[
                                                    "this field is required",
                                                    "email is not valid"
                                                ]}
                                            />
                                            <TextValidator
                                                className="mb-6 w-full"
                                                variant="outlined"
                                                label="Status"
                                                onChange={this.handleInputChange}
                                                type="text"
                                                name="active"
                                                value={this.state.active}
                                                validators={["required"]}
                                                errorMessages={["this field is required"]}
                                            />
                                            <TextValidator
                                                className="mb-6 w-full"
                                                variant="outlined"
                                                label="Date"
                                                type="date"
                                                value={ moment(this.state.date_expiration).format("YYYY-MM-DD") }
                                                onChange={this.handleInputChange}
                                                name="date_expiration"
                                                validators={["required", "is Date"]}
                                                errorMessages={[
                                                    "this field is required",
                                                    "Date is not valid"
                                                ]}
                                            />

                                            <FormControlLabel
                                                className="mb-4"
                                                name="agreement"
                                                onChange={this.handleChange}
                                                control={<Checkbox />}
                                                label="I have read and agree to the terms of service."
                                            />
                                            <div className="flex items-center">
                                                <Button
                                                    className="capitalize"
                                                    variant="contained"
                                                    color="primary"
                                                    type="submit"

                                                    onClick={this.onSubmit}
                                                >
                                                   <EditIcon>Edit</EditIcon>
                                                </Button>

                                            </div>
                                        </ValidatorForm>
                                    </div>
                                </Grid>
                            </Grid>
                        </Card>
                    </div>
                </div>
            </div>
        );
    }
}

export default TableAddApp;