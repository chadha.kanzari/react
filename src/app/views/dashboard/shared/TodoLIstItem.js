import React from "react";
import { ListItem, ListItemText, Button } from "@material-ui/core";
import firebase from "../../../services/firebase/firebaseConfig"
import {color} from "echarts/src/export";

export default function TodoListItem({ todo, inprogress, id }) {
    function toggleInProgress() {
        firebase.firestore().collection("todos").doc(id).update({
            inprogress: !inprogress,
        });
    }

    function deleteTodo() {
        firebase.firestore().collection("todos").doc(id).delete();
    }

    return (
        <div style={{ display: "flex" }}>
            <ListItem>
                <ListItemText
                    primary={todo}
                    secondary={inprogress ? "In Progress": "Completed"}
                />
            </ListItem>

            <Button onClick={toggleInProgress} color="primary">
                {inprogress ? "Done" : "UnDone"}
            </Button>
            <Button onClick={deleteTodo} color="secondary"><b>X</b></Button>
        </div>
    );
}
