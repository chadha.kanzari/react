import React from "react";
import TextField from "@material-ui/core/TextField";
import { useState, useEffect } from "react";
import { Button } from "@material-ui/core";
import firebase from "../../../services/firebase/firebaseConfig"
import TodoListItem from "./TodoLIstItem";
import "./Todo.css";
import {Breadcrumb} from "../../../../matx";
function Todojs(){
    const [todos, setTodos] = useState([]);
    const [todoInput, setTodoInput] = useState("");
    useEffect(() => {
        getTodos();
    }, []); // blank to run only on first launch
    function getTodos() {
        firebase.firestore().collection("todos").onSnapshot(function (querySnapshot) {
            setTodos(
                querySnapshot.docs.map((doc) => ({
                    id: doc.id,
                    todo: doc.data().todo,
                    inprogress: doc.data().inprogress,
                }))
            );
        });
    }
    function addTodo(e) {
        e.preventDefault();

        firebase.firestore().collection("todos").add({
            inprogress: true,
            timestamp: firebase.firestore.FieldValue.serverTimestamp(),
            todo: todoInput,
        });

        setTodoInput("");
    }
    return(
        <div  className="mb-sm-30">
            <br></br><br></br><br></br><br></br><br></br>
            <Breadcrumb
                routeSegments={[
                    { name: "Manage Tasks", path: "/Tasks" },
                    { name: "Add Todo " }
                ]}
            /><br></br><br></br>
        <div className="App">
            <div
                style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                    width: "100%",
                }}
            >
                <h3 > Todos Tasks ⚡ </h3>
                <form>
                    <TextField
                        id="standard-basic"
                        label="Write a Todo"
                        value={todoInput}
                        style={{ width: "100vw", maxWidth: "500px" }}
                        onChange={(e) => setTodoInput(e.target.value)}
                    />
                    <Button
                        type="submit"
                        variant="contained"
                        onClick={addTodo}
                        style={{ display: "none" }}
                    >
                        Default
                    </Button>
                </form>

                <div style={{ width: "90vw", maxWidth: "500px", marginTop: "24px" }}>
                    {todos.map((todo) => (
                        <TodoListItem
                            todo={todo.todo}
                             inprogress={todo.inprogress}
                            id={todo.id}
                        />
                    ))}
                </div>
            </div>
        </div>
        </div>
    );
}
export default Todojs