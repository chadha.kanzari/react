import React, { Component } from "react";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import {
  Button,
  Icon,
  Grid,
  Radio,
  RadioGroup,
  FormControlLabel,
  Checkbox
} from "@material-ui/core";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from "@material-ui/pickers";
import "date-fns";
import DateFnsUtils from "@date-io/date-fns";
import * as firebase from "firebase";
import localStorageService from "../../../services/localStorageService";

class SimpleForm extends Component {
  state = {
  };

  componentDidMount() {
    this.state= JSON.parse( localStorage.getItem("service_user"));
      console.log(this.state.email);
      // custom rule will have name 'isPasswordMatch'
    ValidatorForm.addValidationRule("isPasswordMatch", value => {
      if (value !== this.state.password) {
        return false;
      }
      return true;
    });
  }

  componentWillUnmount() {
    // remove rule when it is not needed
    ValidatorForm.removeValidationRule("isPasswordMatch");
  }

  handleSubmit = event => {
    const user = firebase.auth().currentUser;
    const pwd=this.state.password;
    user.updatePassword(pwd).then(function() {
      // Update successful.
      localStorageService.setItem("service_user",firebase.auth().currentUser);
      alert("Paswword updated with success");
    }).catch(function(error) {
      // An error happened.
      alert("Error updating password");
      console.log(error)
    });
    // console.log("submitted");
    // console.log(event);
  };

  handleChange = event => {
    event.persist();
    this.setState({ [event.target.name]: event.target.value });
  };

  handleDateChange = date => {
    // console.log(date);

    this.setState({ date });
  };
  render() {
    return (
      <div>
        <ValidatorForm
          ref="form"
          onSubmit={this.handleSubmit}
          onError={errors => null}
        >
          <Grid container spacing={6}>
            <Grid item lg={6} md={6} sm={12} xs={12}>
              <TextValidator
                  className="mb-4 w-full"
                  label="Password"
                  onChange={this.handleChange}
                  name="password"
                  type="password"
                  value={this.state.password}
                  validators={["required","minStringLength: 6",]}
                  errorMessages={["this field is required"]}
              />
              <TextValidator
                  className="mb-4 w-full"
                  label="Confirm Password"
                  onChange={this.handleChange}
                  name="confirmPassword"
                  type="password"
                  value={this.state.confirmPassword}
                  validators={["required", "isPasswordMatch"]}
                  errorMessages={[
                    "this field is required",
                    "password didn't match"
                  ]}
              />

            </Grid>
          </Grid>
          <Button color="primary" variant="contained" type="submit">
            <Icon>send</Icon>
            <span className="pl-2 capitalize">Submit</span>
          </Button>
        </ValidatorForm>
      </div>
    );
  }
}

export default SimpleForm;
