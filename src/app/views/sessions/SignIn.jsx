import React, { Component } from "react";
import {
  Card,
  Checkbox,
  FormControlLabel,
  Grid,
  Button,
  CircularProgress
} from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";

//import { loginWithEmailAndPassword } from "../../services/firebase/firebaseAuthService";
//import FirebaseAuthService from "../../services/firebase/firebaseAuthService";
import {firebaseLoginEmailPassword} from "../../redux/actions/LoginActions";
import firebase from "firebase/app";

const styles = theme => ({
  wrapper: {
    position: "relative"
  },

  buttonProgress: {
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12
  }
});

class SignIn extends Component {
  state = {
  };



  handleChange = event => {
    event.persist();
    this.setState({
      [event.target.name]: event.target.value
    });
  };
  handleFormSubmit = event => {
    //this.props.loginWithEmailAndPassword({ ...this.state });
    this.props.firebaseLoginEmailPassword({ ...this.state })
  // this.props.signInWithPopup("google")

  };

  handleForm2Submit = event => {
    this.props.signInWithPopup('google')
  };

  render() {
    let { email, password } = this.state;
    let { classes } = this.props;
    return (
      <div className="signup flex justify-center w-full h-full-screen">
        <div className="p-8">
          <Card className="signup-card position-relative y-center">
            <Grid container>
              <Grid item lg={5} md={5} sm={5} xs={12}>
                <div className="p-8 flex justify-center items-center h-full">
                  <img src="/assets/images/illustrations/dreamer.svg" alt="" />
                </div>
              </Grid>
              <Grid item lg={7} md={7} sm={7} xs={12}>
                <div className="p-9 h-full bg-light-gray position-relative">
                  <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
                    <TextValidator
                      className="mb-6 w-full"
                      variant="outlined"
                      label="Email"
                      onChange={this.handleChange}
                      type="email"
                      name="email"
                      value={email}
                      validators={["required", "isEmail"]}
                      errorMessages={[
                        "this field is required",
                        "email is not valid"
                      ]}
                    />
                    <TextValidator
                      className="mb-3 w-full"
                      label="Password"
                      variant="outlined"
                      onChange={this.handleChange}
                      name="password"
                      type="password"
                      value={password}
                      validators={["required"]}
                      errorMessages={["this field is required"]}
                    />
                    <FormControlLabel
                      className="mb-3"
                      name="agreement"
                      onChange={this.handleChange}
                      control={<Checkbox  />}
                      label="I  agree to the terms of service."
                      validators={["required"]}
                      errorMessages={["this field is required"]}
                    />
                    <div className="flex flex-wrap items-center mb-4">
                      <div className={classes.wrapper}>
                        <Button
                          variant="contained"
                          color="primary"
                          disabled={this.props.login.loading}
                          type="submit"
                        >
                          Sign in to Enter Dashboard
                        </Button>
                        {this.props.login.loading && (
                          <CircularProgress
                            size={24}
                            className={classes.buttonProgress}
                          />
                        )}
                      </div>

                      <Button
                          className="text-primary"
                          onClick={() =>
                              this.props.history.push("/session/forgot-password")
                          }
                      >
                        Forgot password?
                      </Button>
                    </div>


                  </ValidatorForm>
                  <ValidatorForm ref="form2" onSubmit={this.handleForm2Submit}>




                  </ValidatorForm>


                </div>
              </Grid>
            </Grid>
          </Card>
        </div>
      </div>
    );
  }


}

const mapStateToProps = state => ({
  //loginWithEmailAndPassword: PropTypes.func.isRequired,
  signInWithPopup: PropTypes.func.isRequired,
  firebaseLoginEmailPassword: PropTypes.func.isRequired,
  login: state.login
});
export default withStyles(styles, { withTheme: true })(withRouter(connect(mapStateToProps, { firebaseLoginEmailPassword})(SignIn))
      //{ logi"nWithEmailAndPassword })(SignIn))
);
