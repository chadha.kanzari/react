import React from "react";

const tasksRoutes = [
    {
        path: "/Tasks/ManageTasks",
        component: React.lazy(() => import("./tasks"))
    },

];

export default tasksRoutes;
