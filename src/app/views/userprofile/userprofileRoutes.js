import React from "react";

const userprofileRoutes = [
    {
        path: "/userprofile/Profile",
        component: React.lazy(() => import("./EditProfile")),
    }
];

export default userprofileRoutes;
