import Mock from "../mock";
import shortId from "shortid";

const NotificationDB = {
  list: [
    {
      id: shortId.generate(),
      heading: "Reminder",
      icon: {
        name: "chat",
        color: "primary"
      },
      timestamp: 1570702802573,
      title: "New Admin ? ",
      subtitle: "Please check the admin's List ...",
      path: "customers/AddUserList"
    },
    {
      id: shortId.generate(),
      heading: "Alert",
      icon: {
        name: "notifications",
        color: "error"
      },
      timestamp: 1570702702573,
      title: "Conventions Contacts",
      subtitle: "Check recent Conventions for more details",
      path: "convention/SetConventions"
    },
    {
      id: shortId.generate(),
      heading: "Notice",
      icon: {
        name: "chat",
        color: "primary"
      },
      timestamp: 1570502502573,
      title: "Need to update an application? ",
      subtitle: "Please check the application list ...",
      path: "list"
    }
  ]
};

Mock.onGet("/api/notification").reply(config => {
  const response = NotificationDB.list;
  return [200, response];
});

Mock.onPost("/api/notification/add").reply(config => {
  const response = NotificationDB.list;
  return [200, response];
});

Mock.onPost("/api/notification/delete").reply(config => {
  let { id } = JSON.parse(config.data);
  console.log(id);

  const response = NotificationDB.list.filter(
    notification => notification.id !== id
  );
  NotificationDB.list = [...response];
  return [200, response];
});

Mock.onPost("/api/notification/delete-all").reply(config => {
  NotificationDB.list = [];
  const response = NotificationDB.list;
  return [200, response];
});
