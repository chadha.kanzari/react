import React from "react";

const scrumboardRoutes = [
    {
        path: "/db/scrumboard",
        component: React.lazy(() => import("./scrumBoard"))
    }
];

export default scrumboardRoutes;